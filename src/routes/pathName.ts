export const PATH_NAME = {
	ROOT: '/',
	ABOUT: '/about',
	WORKS: '/works',
	PORTFOLIO: '/portfolio',
	CONTACT: '/contact',
	ALLCERTIFICATION: '/about/allcertifications',
	BLOG: 'https://technology-sharing.vercel.app/',
}
