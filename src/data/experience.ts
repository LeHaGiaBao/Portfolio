import wds from '@/assets/Experience/wds.png'
import uit from '@/assets/Experience/uit.png'
import nbk from '@/assets/Experience/nbk.jpg'
import spiraledge from '@/assets/Experience/spiraledge.png'

export const experience = [
	{
		id: 1,
		ExID: 1,
		image: nbk,
		title: 'High School Student',
		organization: 'Nguyen Binh Khiem High School for the gifted | Vinh Long City',
		time: 'August 2017 - August 2020',
	},
	{
		id: 2,
		ExID: 2,
		image: uit,
		title: 'E-commerce Student',
		organization:
			'University of Information Technology (Viet Nam National University Ho Chi Minh City)',
		time: 'October 2020 - Present',
	},
	{
		id: 3,
		ExID: 3,
		image: wds,
		title: 'Collaborator',
		organization: 'WebDev Studios',
		time: 'November 2020 - March 2021',
	},
	{
		id: 4,
		ExID: 4,
		image: wds,
		title: 'Member',
		organization: 'WebDev Studios',
		time: 'March 2021 - Present',
	},
	{
		id: 5,
		ExID: 5,
		image: wds,
		title: 'Frontend Web Developer',
		organization: 'WebDev Studios',
		time: 'January 2022 - Present',
	},
	{
		id: 6,
		ExID: 6,
		image: wds,
		title: 'Deputy of Programming',
		organization: 'WebDev Studios',
		time: 'November 2022 - September 2023',
	},
	{
		id: 7,
		ExID: 7,
		image: spiraledge,
		title: 'Frontend Developer',
		organization: 'Spiraledge Vietnam',
		time: 'July 2023 - October 2023 (Internship)',
	},
]
