import Navbar from '@/layouts/Navbar'
import Footer from '@/layouts/Footer'
import ComingSoon from '@/components/ComingSoon/ComingSoon'

export default function PortfolioId() {
    return (
        <>
            <Navbar />
            <ComingSoon />
            <Footer />
        </>
    )
}